/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unapec.services;

import com.unapec.dao.EntradaContableDAO;
import com.unapec.modelos.EntradaContable;
import com.unapec.modelos.EntradaContableInput;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author efraintoribioreyes
 */
@Path("/ContabilidadWS")
public class ContabilidadService {

    EntradaContableDAO dao = new EntradaContableDAO();

    @POST
    @Path("/registrarAsientos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registrarAsientos(EntradaContableInput entradasContablesInput) {
        try {
            int asientoId = dao.registrarEntradaContable(entradasContablesInput.getEntradasContables());
            return Response.status(Response.Status.OK).entity("Asiento registrado satisfactoriamente con el id: " + asientoId).build();
        } catch (SQLException ex) {
            Logger.getLogger(ContabilidadService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

}
