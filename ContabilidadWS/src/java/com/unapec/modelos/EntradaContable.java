/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unapec.modelos;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author efraintoribioreyes
 */
@XmlRootElement(name = "entradaContable")
public class EntradaContable implements Serializable {
    @XmlElement(name = "descripcion")
    private String descripcion;
    @XmlElement(name = "auxiliar")
    private Integer auxiliar;
    @XmlElement(name = "cuenta")
    private Integer cuenta;
    @XmlElement(name = "tipoMovimiento")
    private String tipoMovimiento;
    @XmlElement(name = "monto")
    private Double monto;
    @XmlElement(name = "moneda")
    private Integer moneda;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(Integer auxiliar) {
        this.auxiliar = auxiliar;
    }

    public Integer getCuenta() {
        return cuenta;
    }

    public void setCuenta(Integer cuenta) {
        this.cuenta = cuenta;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Integer getMoneda() {
        return moneda;
    }

    public void setMoneda(Integer moneda) {
        this.moneda = moneda;
    }
}
