/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unapec.modelos;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author efraintoribioreyes
 */
@XmlRootElement(name = "entradaContableRequest")
public class EntradaContableInput implements Serializable {
    
    @XmlElement(name = "entradasContables")
    private List<EntradaContable> entradasContables;

    public List<EntradaContable> getEntradasContables() {
        return entradasContables;
    }

    public void setEntradasContables(List<EntradaContable> entradasContables) {
        this.entradasContables = entradasContables;
    }
}
