/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unapec.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author efraintoribioreyes
 */
public class DAO {

    private static final String DB_CONNECTION = "jdbc:sqlserver://contabilidadintegracion2.database.windows.net:1433;"
            + "database=IntegracionContabilidad2;"
            + "user=contabilidadadmin@contabilidadintegracion2;"
            + "password=Contabilidad*12;"
            + "encrypt=true;"
            + "trustServerCertificate=false;"
            + "hostNameInCertificate=*.database.windows.net;"
            + "loginTimeout=30;";

    protected Connection getConnection() throws ClassNotFoundException {
        Connection dbConnection = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            dbConnection = DriverManager.getConnection(
                    DB_CONNECTION);
            return dbConnection;

        } catch (SQLException e) {
            System.out.println("Error al realizar connecion a BD: " + e.getMessage());
        }

        return dbConnection;
    }
}
