/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unapec.dao;

import com.unapec.modelos.EntradaContable;
import com.unapec.modelos.SalidaContable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author efraintoribioreyes
 */
public class SalidaContableDAO extends DAO {
    
    public List<SalidaContable> consultarAsientosREgistrados() throws SQLException {
        List<SalidaContable> resultado = new ArrayList<SalidaContable>();
        
        Statement statement = null;
        PreparedStatement statementEntradas = null;
        Connection dbConnection = null;
        String queryAsientos = "SELECT AsientoContableID, Descripcion, AuxiliarID, Fecha, case when EstadoID = 1 end as Estado, "
                + "case when TipoMonedaID = 1 then 'Peso' when TipoMonedaID = 2 then 'Dollar' when TipoMonedaID =  3 then 'Euro' end as TipoMoneda "
                + " FROM AsientoContable";  
        String queryEntradas = "SELECT Monto, CuentaContableID, TipoMovimiento FROM EntradaContable WHERE AsientoContableID = ? ";
        
        try {
            dbConnection = getConnection();
            statement = dbConnection.createStatement();
            statementEntradas = dbConnection.prepareStatement(queryEntradas);
            ResultSet rs = statement.executeQuery(queryAsientos);
            
            while(rs.next()) {
                int asientoId = rs.getInt("AsientoContableID");
                String descripcionAsiento = rs.getString("Descripcion");
                int auxiliarId = rs.getInt("AxiliarID");
                Date fechaAsiento = rs.getDate("Fecha");
                String estado = rs.getString("Estado");
                String tipoMoneda = rs.getString("TipoMoneda");
                
                statementEntradas.setInt(1, asientoId);
                ResultSet rsEntradas = statementEntradas.executeQuery();
                
                while (rsEntradas.next()) {
                    SalidaContable salidaContable = new SalidaContable();
                }
            }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EntradaContableDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }
        }        
        
        return resultado;
    }
    
}
