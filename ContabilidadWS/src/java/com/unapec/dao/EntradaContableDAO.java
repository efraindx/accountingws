package com.unapec.dao;

import com.unapec.modelos.EntradaContable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author efraintoribioreyes
 */
public class EntradaContableDAO extends DAO {

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public int registrarEntradaContable(List<EntradaContable> entradasContables) throws SQLException {

        EntradaContable primeraEntrada = entradasContables.get(0);
        String descripcionAsiento = primeraEntrada.getDescripcion();
        Integer auxilarId = primeraEntrada.getAuxiliar();
        Integer monedaId = primeraEntrada.getMoneda();

        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        int asientoId = 0;

        try {
            dbConnection = getConnection();

            String insertAsiento = "INSERT INTO AsientoContable"
                    + "(Descripcion, AuxiliarID, Fecha, EstadoID, TipoMonedaID) VALUES"
                    + "(?,?,?,?,?); SELECT SCOPE_IDENTITY()";
            preparedStatement = dbConnection.prepareStatement(insertAsiento);

            preparedStatement.setString(1, descripcionAsiento);
            preparedStatement.setInt(2, auxilarId);
            preparedStatement.setTimestamp(3, getCurrentTimeStamp());
            preparedStatement.setString(4, "R");
            preparedStatement.setInt(5, monedaId);

            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            asientoId = rs.getInt(1);
            
            String insertEntradaContable = "INSERT INTO EntradaContable "
                         + "(AsientoContableID, Monto, CuentaContableID, TipoMovimiento)"
                         + "values (?, ?, ?, ?)";

            for (EntradaContable entradaContable : entradasContables) {
                 PreparedStatement entradaContableStatement = dbConnection.prepareStatement(insertEntradaContable);
                 entradaContableStatement.setInt(1, asientoId);
                 entradaContableStatement.setDouble(2, entradaContable.getMonto());
                 entradaContableStatement.setInt(3, entradaContable.getCuenta());
                 entradaContableStatement.setString(4, entradaContable.getTipoMovimiento());
                 
                 //Registrar entradas asiento
                 entradaContableStatement.executeUpdate();
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EntradaContableDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }
        }

        return asientoId;
    }

    private static java.sql.Timestamp getCurrentTimeStamp() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Timestamp(today.getTime());
    }
}
